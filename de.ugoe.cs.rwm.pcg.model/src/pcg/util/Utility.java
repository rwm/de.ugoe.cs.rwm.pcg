package pcg.util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import pcg.PcgPackage;

public class Utility {

	/**
	 * Loads PCG model as List of EObjects.
	 *
	 * @param path
	 *            to the PCG model
	 * @return PCG model as List of EObjects
	 */
	public static EList<EObject> loadPCG(Path pcgPath) {
		PcgPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("pcg", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		// URI fileURI = URI.createURI(pcgPath.toString());
		URI fileURI = URI.createFileURI(pcgPath.toString());
		Resource resource = resSet.getResource(fileURI, true);

		return resource.getContents();
	}

	/**
	 * Stores PCG at the given Path path.
	 *
	 * @param path
	 *            Path in which the PCG gets stored.
	 * @param graph
	 *            Toplevel element of the PCG.
	 */
	public static void storePCG(Path path, pcg.Graph graph) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("pcg", new XMIResourceFactoryImpl());

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		// create a resource
		// URI fileURI = URI.createURI(path.toString());
		URI fileURI = URI.createFileURI(path.toString());
		Resource resource = resSet.createResource(fileURI);
		// Get the first model element and cast it to the right type, in my
		// example everything is hierarchical included in this first node
		resource.getContents().add(graph);

		// now save the content.
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
